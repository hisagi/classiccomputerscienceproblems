from typing import Generic, TypeVar, Dict, List, Optional
from abc import ABC, abstractmethod

V = TypeVar('V')
D = TypeVar('D')


# 全制約の基底クラス
class Constraint(Generic[V, D], ABC):
    # 制約に関わる関数
    def __init__(self, variables: List[V]) -> None:
        self.variables = variables

    # サブクラスでオーバライドしないといけない
    @abstractmethod
    def satisfied(self, assignment: Dict[V, D]) -> bool:
        ...


# 制約充足問題は型Dの領域の型Vの変数と
# 変数の領域選択が正当か決定する制約からなる
class CSP(Generic[V, D]):
    def __init__(self, variables: List[V], domains: Dict[V, List[D]]) -> None:
        self.variables: List[V] = variables  # 制約対象変数
        self.domains: Dict[V, List[D]] = domains  # 変数の領域
        self.constraints: Dict[V, List[Constraint[V, D]]] = {}
        # 変数に対応する制約リストの初期化と、選択領域があるかの確認
        for variable in self.variables:
            self.constraints[variable] = []
            if variable not in self.domains:
                raise LookupError("Every variable should have a domain assigned to it.")  # noqa

    def add_constraint(self, constraint: Constraint[V, D]) -> None:
        for variable in constraint.variables:
            if variable not in self.variables:
                raise LookupError("Variable in constraint not in CSP")
            else:
                self.constraints[variable].append(constraint)

    # 変数の全制約をチェックして値アサインメントが無矛盾かチェック
    def constraint(self, variable: V, assignment: Dict[V, D]) -> bool:
        for constraint in self.constraints[variable]:
            if not constraint.satisfied(assignment):
                return False
        return True

    def backtracking_search(self, assignment: Dict[V, D] = {}) -> Optional[Dict[V, D]]: # noqa
        # 全変数がアサインメントされていればアサインメントは完全（基底部）
        if len(assignment) == len(self.variables):
            return assignment

        # アサインメントにないCSPの全変数を取得
        unassigned: List[V] = [v for v in self.variables if v not in assignment]  # noqa

        # 最初の未アサイン変数の可能領域値すべてを取得
        first: V = unassigned[0]
        for value in self.domains[first]:
            # Pythonは参照渡しのため、引数で渡されたアサインをローカル用にコピー
            local_assignment = assignment.copy()
            # 取り敢えず値を設定してみる
            local_assignment[first] = value
            # 矛盾がないなら再起（継続）
            if self.constraint(first, local_assignment):
                result: Optional[Dict[V, D]] = self.backtracking_search(local_assignment)  # noqa
                # 結果がNoneだとバックトラック
                if result is not None:
                    return result
        return None
