from __future__ import annotations


class Bits():
    def __init__(self, init: int = 1):
        self.bits = init

    def left_shift(self, n: int) -> Bits:
        return Bits(self.bits << n)

    def right_shift(self, n: int) -> Bits:
        return Bits(self.bits >> n)

    def bits_and(self, operand: Bits) -> Bits:
        return Bits(self.bits & operand.bits)

    def bits_or(self, operand: Bits) -> Bits:
        return Bits(self.bits | operand.bits)

    def bits_not(self) -> None:
        return Bits(~self.bits)

    def bits_xor(self, operand: Bits) -> Bits:
        return Bits(self.bits ^ operand.bits)

    def bits_length(self) -> int:
        return self.bits.bit_length()

    def __getitem__(self, x: int) -> Bits:
        return self.right_shift(x).bits_and(Bits(1))

    def __eq__(self, x: Bits) -> bool:
        return self.bits == x.bits


class CompressedGene:
    def __init__(self, gene: str) -> None:
        self._compress(gene)

    def _compress(self, gene: str) -> None:
        self.bits: Bits = Bits(1)  # 番兵で開始
        for nucleotide in gene.upper():
            self.bits = self.bits.left_shift(2)
            if nucleotide == "A":
                self.bits = self.bits.bits_or(Bits(0))
            elif nucleotide == "C":
                self.bits = self.bits.bits_or(Bits(1))
            elif nucleotide == "G":
                self.bits = self.bits.bits_or(Bits(2))
            elif nucleotide == "T":
                self.bits = self.bits.bits_or(Bits(3))
            else:
                raise ValueError("Invalid Nucleotide:{}".format(nucleotide))

    def decompress(self) -> str:
        gene: str = ""
        for i in range(0, self.bits.bits_length() - 1, 2):
            bits: Bits = self.bits[i + 1].left_shift(1).bits_or(self.bits[i])
            if bits == Bits(0):
                gene += "A"
            elif bits == Bits(1):
                gene += "C"
            elif bits == Bits(2):
                gene += "G"
            elif bits == Bits(3):
                gene += "T"
            else:
                raise ValueError("Invalid bits:{}".format(bits))
        return gene[::-1]  # [::-1]は逆向きスライスで文字列を反転

    def __str__(self) -> str:
        return self.decompress()


if __name__ == "__main__":
    from sys import getsizeof
    original: str = \
        "TAGGGATTAACCGTTATATATATATATAGCCATGGATCGATTTTATATATAGGGATATTTACCCCCGTTAATATATATATGCCATCG" * 100  # noqa
    print("original is {} bytes".format(getsizeof(original)))
    compressed: CompressedGene = CompressedGene(original)
    print("compressed is {} bytes".format(getsizeof(compressed)))
    print(compressed)
    print("original and decompressed are the same: {}".format(
        original == compressed.decompress()))
