from typing import List


# 自分で考えた方法で、第n項のフィボナッチ数を出力する
def fib(n: int) -> int:
    def iter(n: int, result: List[int] = [0, 1]) -> List[int]:
        if n < 2:
            return result
        else:
            return iter(n - 1, result + [result[-2] + result[-1]])
    return iter(n)[-1]


if __name__ == "__main__":
    print(fib(10))
