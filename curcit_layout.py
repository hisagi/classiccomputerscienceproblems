from typing import NamedTuple, List, Dict, Optional
from csp import CSP, Constraint


class Part(NamedTuple):
    name: str
    width: int
    height: int


Grid = List[List[Part]]  # マス目の型エイリアス


class GridLocation(NamedTuple):
    row: int
    column: int


def generate_grid(rows: int, columns: int) -> Grid:
    # マス目を0で初期化
    return [["_" for c in range(columns)] for r in range(rows)]


def display_grid(grid: Grid) -> None:
    for row in grid:
        print("".join(row))


def generate_domain(part: Part, grid: Grid) -> List[List[GridLocation]]:
    domain: List[List[GridLocation]] = []
    height: int = len(grid)
    width: int = len(grid[0])
    part_height: int = part.height
    part_width: int = part.width
    for row in range(height):
        for col in range(width):
            # 横置き
            if col + part_width <= width and row + part_height <= height:
                columns: range = range(col, col + part_width)
                rows: range = range(row, row + part_height)
                domain.append([GridLocation(r, c) for c in columns for r in rows])
            # 縦置き
            if col + part_height <= width and row + part_width <= height:
                columns: range = range(col, col + part_height)
                rows: range = range(row, row + part_width)
                domain.append([GridLocation(r, c) for c in columns for r in rows])
    return domain


class CircuitLayoutConstraint(Constraint[str, List[GridLocation]]):
    def __init__(self, parts: List[Part]) -> None:
        super().__init__(parts)
        self.parts: List[Part] = parts

    def satisfied(self, assinment: Dict[Part, List[GridLocation]]) -> bool:
        # マス目の位置に重複があれば冗長
        all_locations = [locs for values in assinment.values() for locs in values]
        return len(set(all_locations)) == len(all_locations)


if __name__ == "__main__":
    grid: Grid = generate_grid(9, 9)
    parts: List[Part] = [Part("A", 1, 6), Part("B", 4, 4), Part("C", 3, 3), Part("D", 2, 2), Part("E", 5, 2)]
    locations: Dict[Part, List[List[GridLocation]]] = {}
    for part in parts:
        locations[part] = generate_domain(part, grid)
    csp: CSP[Part, List[GridLocation]] = CSP(parts, locations)
    csp.add_constraint(CircuitLayoutConstraint(parts))
    solution: Optional[Dict[Part, List[GridLocation]]] = csp.backtracking_search()
    if solution is None:
        print("No solution found!")
    else:
        for part, grid_locations in solution.items():
            for grid_location in grid_locations:
                grid[grid_location.row][grid_location.column] = part.name
        display_grid(grid)
