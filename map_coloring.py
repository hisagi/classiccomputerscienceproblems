from csp import Constraint, CSP
from typing import Dict, List, Optional


class MapColoringConstraint(Constraint[str, str]):
    def __init__(self, place1: str, place2: str) -> None:
        super().__init__([place1, place2])
        self.place1: str = place1
        self.place2: str = place2

    def satisfied(self, assignment: Dict[str, str]) -> bool:
        # どちらの場所もアサインメントにないと、色は矛盾しない
        if self.place1 not in assignment or self.place2 not in assignment:
            return True
        # place1の色がplace2の色と異なることをチェック
        return assignment[self.place1] != assignment[self.place2]


if __name__ == "__main__":
    variables: List[str] = ["Western Australia", "Northern Territory",
                            "South Australia", "Queensland", "New South Wales",
                            "Victoria", "Tasmania"]
    domains: Dict[str, List[str]] = {}
    adjacent_states: Dict[str, List[str]] = {"Western Australia": ["Northern Territory", "South Australia"],
                                             "South Australia": ["Northern Territory"],
                                             "Queensland": ["Northern Territory", "South Australia", "New South Wales"],
                                             "New South Wales": ["South Australia"],
                                             "Victoria": ["South Australia", "New South Wales", "Tasmania"]}
    for variable in variables:
        domains[variable] = ["red", "green", "blue"]
    csp: CSP[str, str] = CSP(variables, domains)
    # csp.add_constraint(MapColoringConstraint("Western Australia",
    #                                          "Northern Territory"))
    # csp.add_constraint(MapColoringConstraint("Western Australia",
    #                                          "South Australia"))
    # csp.add_constraint(MapColoringConstraint("South Australia",
    #                                          "Northern Territory"))
    # csp.add_constraint(MapColoringConstraint("Queensland",
    #                                          "Northern Territory"))
    # csp.add_constraint(MapColoringConstraint("Queensland",
    #                                          "South Australia"))
    # csp.add_constraint(MapColoringConstraint("Queensland",
    #                                          "New South Wales"))
    # csp.add_constraint(MapColoringConstraint("New South Wales",
    #                                          "South Australia"))
    # csp.add_constraint(MapColoringConstraint("Victoria",
    #                                          "South Australia"))
    # csp.add_constraint(MapColoringConstraint("Victoria",
    #                                          "New South Wales"))
    # csp.add_constraint(MapColoringConstraint("Victoria",
    #                                          "Tasmania"))
    for state, neighbors in adjacent_states.items():
        for neighbor in neighbors:
            csp.add_constraint(MapColoringConstraint(state, neighbor))
    solution: Optional[Dict[str, str]] = csp.backtracking_search()
    if solution is None:
        print("No solution found!")
    else:
        print(solution)
