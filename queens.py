from csp import Constraint, CSP
from typing import Dict, List, Optional


class QueensConstraint(Constraint[int, int]):
    def __init__(self, columns: List[int]) -> None:
        super().__init__(columns)
        self.columns: List[int] = columns

    def satisfied(self, assignment: Dict[int, int]) -> bool:
        for q1c, q1r in assignment.items():  # q1c = クイーン1の列, q1r = クイーン1の行
            for q2c in range(q1c + 1, len(self.columns) + 1):  # q2c = クイーン2の列
                if q2c in assignment:
                    q2r: int = assignment[q2c]  # q2r = クイーン2の行
                    if q1r == q2r:  # 同じ行?
                        return False
                    if abs(q1r - q2r) == abs(q1c - q2c):  # 対角の方向?
                        return False
        return True  # 矛盾しない


if __name__ == "__main__":
    columns: List[int] = [1, 2, 3, 4, 5, 6, 7, 8]
    rows: Dict[int, List[int]] = {}
    for column in columns:
        rows[column] = [1, 2, 3, 4, 5, 6, 7, 8]
    csp: CSP[int, int] = CSP(columns, rows)

    csp.add_constraint(QueensConstraint(columns))
    solution: Optional[Dict[int, int]] = csp.backtracking_search()
    if solution is None:
        print("No solution found!")
    else:
        print(solution)
