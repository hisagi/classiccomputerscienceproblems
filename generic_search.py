from __future__ import annotations
from typing import TypeVar, Iterable, Sequence, Generic, List, Callable, Set, Deque, Dict, Any, Optional
from typing_extensions import Protocol
from heapq import heappush, heappop

T = TypeVar('T')


def linear_contains(iterable: Iterable[T], key: T) -> bool:
    for item in iterable:
        if item == key:
            return True
    return False


C = TypeVar('C', bound="Comparable")


class Comparable(Protocol):
    def __eq__(self, other: Any) -> bool:
        ...

    def __lt__(self: C, other: C) -> bool:
        ...

    def __gt__(self: C, other: C) -> bool:
        return (not self < other) and self != other

    def __le__(self: C, other: C) -> bool:
        return self < other or self == other

    def __ge__(self: C, other: C) -> bool:
        return not self < other


def binary_contains(sequence: Sequence[C], key: C) -> bool:
    low: int = 0
    high: int = len(sequence) - 1
    while low <= high:
        mid: int = (low + high) // 2
        if sequence[mid] < key:
            low = mid + 1
        elif sequence[mid] > key:
            high = mid - 1
        else:
            return True
    return False


class Stack(Generic[T]):
    def __init__(self) -> None:
        self._container: List[T] = []

    @property
    def empty(self) -> bool:
        return not self._container  # コンテナが空ならば真とする

    def push(self, item: T) -> None:
        self._container.append(item)

    def pop(self) -> T:
        return self._container.pop()  # LIFO

    def __repr__(self) -> str:
        return repr(self._container)


class Node(Generic[T]):
    def __init__(self, state: T, parent: Optional[Node], cost: float = 0.0,
                 heuristic: float = 0.0) -> None:
        self.state: T = state
        self.parent: Optional[Node] = parent
        self.cost: float = cost
        self.heuristic: float = heuristic

    def __lt__(self, other: Node) -> bool:
        return (self.cost + self.heuristic) < (other.cost + other.heuristic)


def dfs(initial: T, goal_test: Callable[[T], bool],
        successors: Callable[[T], List[T]]) -> Optional[Node[T]]:
    # frontierはまだ行っていないところ
    frontier: Stack[Node[T]] = Stack()
    frontier.push(Node(initial, None))
    # exploredは行ったところ
    explored: Set[T] = {initial}

    # 調べるところがある限り調べる
    while not frontier.empty:
        current_node: Node[T] = frontier.pop()
        current_state: T = current_node.state
        # ゴールに来たら終わり
        if goal_test(current_state):
            return current_node
        # まだ行っていない、次に行くところをチェック
        for child in successors(current_state):
            if child in explored:  # すでに調べた子はスキップ
                continue
            explored.add(child)
            frontier.push(Node(child, current_node))
    return None  # すべて調べたのにゴールはなかった


def node_to_path(node: Node[T]) -> List[T]:
    path: List[T] = [node.state]
    # 終端から戦闘へ逆向きに進む
    while node.parent is not None:
        node = node.parent
        path.append(node.state)
    path.reverse()
    return path


class Queue(Generic[T]):
    def __init__(self) -> None:
        self._container: Deque[T] = Deque()

    @property
    def empty(self) -> bool:
        return not self._container  # コンテナが空ならば真となる

    def push(self, item: T) -> None:
        self._container.append(item)

    def pop(self) -> T:
        return self._container.popleft()  # FIFO

    def __repr__(self) -> str:
        return repr(self._container)


def bfs(initial: T, goal_test: Callable[[T], bool],
        successors: Callable[[T], List[T]]) -> Optional[Node[T]]:
    # frontierはまだ行っていないところ
    frontier: Queue[Node[T]] = Queue()
    frontier.push(Node(initial, None))
    # exploredは行ったところ
    explored: Set[T] = {initial}

    # 調べるところがある限り続ける
    while not frontier.empty:
        current_node: Node[T] = frontier.pop()
        current_state: T = current_node.state
        # ゴールに達したら終わり
        if goal_test(current_state):
            return current_node
        # まだ行っていない、次に行くところをチェック
        for child in successors(current_state):
            if child in explored:
                continue
            explored.add(child)
            frontier.push(Node(child, current_node))
    return None  # すべて調べたのにゴールはなかった


class PriorityQueue(Generic[T]):
    def __init__(self) -> None:
        self._container: List[T] = []

    @property
    def empty(self) -> bool:
        return not self._container  # コンテナが空ならnotでtrue

    def push(self, item: T) -> None:
        heappush(self._container, item)  # 優先度は要素の順序で決定

    def pop(self) -> T:
        heappop(self._container)  # 優先度順で取り出す

    def __repr__(self) -> str:
        return repr(self._container)


def aster(initial: T, goal_test: Callable[[T], bool],
          successors: Callable[[T], List[T]],
          heuristic: Callable[[T], float]) -> Optional[Node[T]]:
    # frontierはまだ行っていないところ
    frontier: PriorityQueue[Node[T]] = Queue()
    frontier.push(Node(initial, None, 0.0, heuristic(initial)))
    # exploredは行ったところ
    explored: Dict[T, float] = {initial: 0.0}

    # 調べるところがある限り続ける
    while not frontier.empty:
        current_node: Node[T] = frontier.pop()
        current_state: T = current_node.state
        # ゴールに到達したら終わり
        if goal_test(current_state):
            return current_node
        # まだ行っていない、次に行くところをチェック
        for child in successors(current_state):
            new_cost: float = current_node.cost + 1
            # 1は格子を家庭。もっと高度なアプリケーションにはコスト関数が必要

            if child not in explored or explored[child] > new_cost:
                explored[child] = new_cost
                frontier.push(Node(child, current_node, new_cost,
                                   heuristic(child)))
    return None  # すべて調べたのにゴールはなかった


if __name__ == "__main__":
    print(linear_contains([1, 5, 15, 15, 15, 15, 20], 5))
    print(linear_contains(["a", "d", "e", "f", "z"], "f"))
    print(linear_contains(["jhon", "mark", "ronald", "sarah"], "sheila"))
